Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ibus-table-others
Source: https://github.com/moebiuscurve/ibus-table-others
Comment:
 This work was packaged for Debian by:
 Asias He <asias.hejun@gmail.com> on Sat, 07 Aug 2010 09:03:20 +0800
 .
 Full table contributor information attached below (from AUTHORS file):
 .
 Compose - Yu Yuwei (acevery) <acevery at gmail.com>
 ipa-x-sampa - Mike Fabian <mfabian at suse.de>
 Cyrillic Transliterated - Daniil Ivanov <daniil.ivanov at gmail.com>
 Russian Traditional - Yuri Victorovich <yuri at rawbw.com>
 Yawerty - Matthew Fischer <futhark at users.sourceforge.net>
 Thai and Viqr - Jens Petersen <petersen at redhat.com>
 Latex - Joerg Haustein <jchaustein at gmx.de>
 CNS11643 - Chinese Foundation For Digitization Technology
  (CMEX財團法人中文數位化技術推廣基金會) http://www.cmes.org.tw/
 Emoji - Aron Xu < @ag108lau AT twitter> (Design)
         Shellex Y <5h3ll3x at gmail.com> (Convert)
 Mathwriter - Naveen Kumar <nkumar@redhat.com>
 Telex and VNI - Nguyễn Gia Phong <vn.mcsinyx at gmail.com>
 .
 For excluded m4/as-version.m4 directory: Copyright 2006
 Thomas Vander Stichele <thomas at apestaart dot org>, used by
 numerous FLOSS projects, yet license unknown. Excluded here
 since the file is not actually used during build.

Files: *
Copyright:
 2009-2010 Caius 'kaio' Chance <me at kaio.net>
License: GPL-3.0-only
Comment:
 See README file.

Files: debian/*
Copyright:
 2010 Asias He <asias.hejun@gmail.com>
License: GPL-3.0-only

Files:
 Makefile.am
 configure.ac
 icons/*
 m4/Makefile.am
 tables/Makefile.am
Copyright:
 2009-2010 Caius 'kaio' Chance <me at kaio.net>
License: GPL-3.0-or-later

Files:
 emoticon-src/*
 tables/emoji-table.txt.data
Copyright:
 Aron Xu < @ag108lau AT twitter> (Design)
 Shellex Y <5h3ll3x at gmail.com> (Convert)
License: GPL-3.0-only

Files: tables/cns11643.txt
Copyright:
 Chinese Foundation For Digitization Technology
  (CMEX財團法人中文數位化技術推廣基金會) http://www.cmes.org.tw/
License: LGPL-2.1-or-later

Files: tables/compose.txt
Copyright: Yu Yuwei (acevery) <acevery at gmail.com>
License: LGPL-2.1-or-later

Files: tables/emoticon-table.txt
Copyright:
 Aron Xu < @ag108lau AT twitter> (Design)
 Shellex Y <5h3ll3x at gmail.com> (Convert)
License: LGPL-2.1-or-later

Files: tables/hu-old-hungarian-rovas.txt
Copyright:
 2016 Mike FABIAN <maiku.fabian@gmail.com>
License: GPL-3.0-or-later

Files: tables/ipa-x-sampa.txt
Copyright:
 Mike Fabian <mfabian at suse.de>
License: GPL-3.0-or-later

Files: tables/latex.txt
Copyright: Joerg Haustein <jchaustein at gmx.de>
License: LGPL-2.1-or-later

Files: tables/mathwriter-ibus.txt
Copyright:
 Naveen Kumar <nkumar@redhat.com>, <nav007@gmail.com>
License: LGPL-2.1-or-later

Files: tables/mongol_bichig.txt
Copyright:
 Popolon <popolon@popolon.org>
 2004 Sam Hocevar <sam@hocevar.net>
License: WTFPL

Files:
 tables/rusle.txt
 tables/rustrad.txt
Copyright: Yuri Victorovich <yuri at rawbw.com>
License: LGPL-2.1-or-later

Files: tables/telex.txt
Copyright: Nguyễn Gia Phong <vn.mcsinyx at gmail.com>
License: GPL-3.0-or-later

Files: tables/thai.txt
Copyright: Jens Petersen <petersen at redhat.com>
License: LGPL-2.1-or-later

Files:
 tables/translit-ua.txt
 tables/translit.txt
Copyright: Daniil Ivanov <daniil.ivanov@gmail.com>
License: LGPL-2.1-or-later

Files: tables/viqr.txt
Copyright: Jens Petersen <petersen at redhat.com>
License: LGPL-2.1-or-later

Files: tables/vni.txt
Copyright: Nguyễn Gia Phong <vn.mcsinyx at gmail.com>
License: GPL-3.0-or-later

Files: tables/yawerty.txt
Copyright: Matthew Fischer <futhark at users.sourceforge.net>
License: LGPL-2.1-or-later

License: GPL-3.0-only
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-3.0-or-later
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: WTFPL
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004
 .
 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.
 .
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
  0. You just DO WHAT THE FUCK YOU WANT TO.

License: LGPL-2.1-or-later
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1’.
